var contador = 0;



function randomCard() {
    let ramdomValue = Math.random()
    let value = 0
    if (ramdomValue <= 0.33) {
        value = 1
    } else if (ramdomValue > 0.33 || ramdomValue <= 0.65) {
        value = 2
    } else { value = 3 }

    switch (value) {
        case 1: return "./img/rock.png"; break;
        case 2: return "./img/paper.png"; break;
        case 3: return "./img/scissor.png"; break;
    }
}

function gameResult(player, enemy) {

    let playerChoice = player.match(/paper|rock|scissor/g)[0]
    let enemyChoice = enemy.match(/paper|rock|scissor/g)[0]
    console.log(playerChoice, enemyChoice)
    //Retorna a imagem ao inves de só palavra
    if (playerChoice === enemyChoice) {
        return "./img/empate.png" //"draw"
    }
    else if (playerChoice === "rock") {
        switch (enemyChoice) {
            case "paper":
                return "./img/derrota.png"//Loose
                break;
            case "scissor":
                return "./img/Victory.png" //win
                break;
        }
    } else if (playerChoice === "paper") {
        switch (enemyChoice) {
            case "scissor":
                return "./img/derrota.png"//Loose
                break;
            case "rock":
                return "./img/Victory.png"  //win
                break;
        }
    }
    else if (playerChoice === "scissor") {
        switch (enemyChoice) {
            case "rock":
                return "./img/derrota.png"//Loose
                break;
            case "paper":
                return "./img/Victory.png" //win
                break;
        }
    }

}




//CRIAÇÃO DO JOGO
let bodyElement = document.querySelector('body')

let gameDivContainer = document.getElementById('gameContainer')
gameDivContainer.className = "gameContainer"

let scenario = document.createElement('video')
scenario.src = "./video/arenaLoop.mp4";
scenario.className = "scenario"
scenario.loop = true
scenario.play()

gameDivContainer.appendChild(scenario)

let divContainer = document.createElement('div')
divContainer.id = "cardsContainer"

function createCards(divID, buttonID, image) {

    let divElement = document.createElement('div')
    divElement.id = `${divID}`
    divElement.className = "Cards"


    let buttonElement = document.createElement('button')
    buttonElement.id = `${buttonID}`


    let imgElement = document.createElement('img')
    imgElement.src = `${image}`

    buttonElement.appendChild(imgElement)
    divElement.appendChild(buttonElement)
    divContainer.appendChild(divElement)
    gameDivContainer.appendChild(divContainer)
    bodyElement.appendChild(gameDivContainer);
}
createCards("rockCard", "rockButton", "./img/rock.png")
createCards("paperCard", "paperButton", "./img/paper.png")
createCards("scissorCard", "scissorButton", "./img/scissor.png")
// APARTI DAQUI OS DIV E BUTTONS JA FORAM CRIADOS

let paperButton = document.getElementById('paperButton');

let rockButton = document.getElementById('rockButton');

let scissorButton = document.getElementById('scissorButton');

//? Rock
rockButton.addEventListener("click", () => {
    console.log('rock')

    rockButton.className = "flip"

    disappear()
    chosenCard("./img/rock.png", randomCard())
})
//? Paper
paperButton.addEventListener("click", () => {
    console.log('paper')
    paperButton.className = "flip"


    disappear()
    chosenCard("./img/paper.png", randomCard())
})

//? Scissor
scissorButton.addEventListener("click", () => {
    console.log('scissor')
    scissorButton.className = "flip"


    disappear()
    chosenCard("./img/scissor.png", randomCard())
})

function disappear() {
    setTimeout(
        () => {
            paperButton.className = "Disappear"
            rockButton.className = "Disappear"
            scissorButton.className = "Disappear"
        }
        , 1300)

    setTimeout(
        () => {
            paperButton.style.display = "none"
            rockButton.style.display = "none"
            scissorButton.style.display = "none"
        }
        , 2000)
}

let words = document.createElement('div')
words.className = "words"
gameDivContainer.appendChild(words)

function createDivImg(className, image, append) {
    let divImgElement = document.createElement('div')
    divImgElement.className = `${className}`
    divImgElement.id = `${className}`
    let imgElement = document.createElement('img')
    imgElement.src = `${image}`
    divImgElement.appendChild(imgElement)
    if (append === "gameDivContainer") {
        gameDivContainer.appendChild(divImgElement)
    }
    if (append === "words") {

        words.appendChild(divImgElement)
    }
}

// Essas duas são direto no gameDivContainer

// ally
// "./img/Mario.png"
createDivImg("ally", "./img/Mario.png", "gameDivContainer")
// enemy
// "./img/Inimigo.png"
createDivImg("enemy", "./img/inimigo.png", "gameDivContainer")

// words - div que guarda todos abaixo
// ---------------------========----------------
// playerChosenCards
// "./img/VersoCarta.png"

createDivImg("playerChosenCards", "./img/VersoCarta.png", "words")

// enemyChosenCards
// "./img/VersoCarta.png"

createDivImg("enemyChosenCards", "./img/VersoCarta.png", "words")

// WordRock
// "./img/pedra.png"

createDivImg("WordRock", "./img/pedra.png", "words")


// WordsScissor
// "./img/tesoura.png"

createDivImg("WordsScissor", "./img/tesoura.png", "words")

// wordResult
// "./img/Victory.png"

createDivImg("wordResult", "", "words")

// WordPaper
// "./img/papel.png"

createDivImg("WordPaper", "./img/papel.png", "words")


let playerChosenCards = document.getElementById('playerChosenCards');

let enemyChosenCards = document.getElementById('enemyChosenCards');

let WordRock = document.getElementById('WordRock');

let WordPaper = document.getElementById('WordPaper');

let WordsScissor = document.getElementById('WordsScissor');

let wordResult = document.getElementById('wordResult');

//VALORES DAS VIDAS E MOEDAS

var moedas = contador * 10

var vidas = 3 
let life1Up = document.createElement('audio')
life1Up.src="./music/1up.wav"
var next = 1
function lifeUp(){
   
    if( Math.floor(moedas/100) === next){
         vidas+=1
       life1Up.play()
       next++
    }
}

function chosenCard(PlayerCard, EnemyCard) {
    console.log(EnemyCard)
    setTimeout(
        () => {
            playerChosenCards.style.opacity = 1;
            enemyChosenCards.style.opacity = 1;
            playerChosenCards.className = "playerChosenCards Appear"
            enemyChosenCards.className = "enemyChosenCards Appear"
        }
        , 2000)

    setTimeout(
        () => {
            WordRock.style.opacity = 1;
            WordRock.className = "WordRock AppearSizing"
        }
        , 3000)

    setTimeout(
        () => {
            WordPaper.style.opacity = 1;
            WordPaper.className = "WordPaper AppearSizing"
        }, 4000)

    setTimeout(
        () => {
            WordsScissor.style.opacity = 1;
            WordsScissor.className = "WordsScissor AppearSizingPlus"
        }, 5000)//animação de 1 seg

    //AQUI É ONDE AS CARTAS VIRAM, LOGO A ANIMAÇÃO DOS PERSONAGENS TAMBEM MUDA
    setTimeout(
        () => {
            playerChosenCards.firstChild.src = `${PlayerCard}`
            enemyChosenCards.firstChild.src = `${EnemyCard}`
        }, 5500)
    setTimeout(
        () => {
            WordRock.className = "WordRock Disappear"
            WordPaper.className = "WordPaper Disappear"
            WordsScissor.className = "WordsScissor Disappear"


        }, 6500)

    setTimeout(
        () => {
            WordRock.style.opacity = 0;
            WordPaper.style.opacity = 0;
            WordsScissor.style.opacity = 0;

            wordResult.firstChild.src = gameResult(PlayerCard, EnemyCard)
        }, 7000)



    setTimeout(
        () => {

           
            wordResult.style.opacity = 1;
            wordResult.className = `wordResult AppearSizingPlus`

            //atribuir valores ao contador de acordo com resultado
            let resultGame = gameResult(PlayerCard, EnemyCard).match(/Victory|derrota|empate/g)[0]
            if (resultGame === "Victory") {
                contador += 1
            } else if (resultGame === "derrota" && vidas > 0) {
                vidas-=1
            }
    
            console.log(contador)

        }, 8000)


    setTimeout(() => resetGame(), 10000)


}



//RESET DO GAME
function resetGame() {
    // Reset das Cartas
    paperButton.className = "Cards"
    rockButton.className = "Cards"
    scissorButton.className = "Cards"

    paperButton.style.display = "flex"
    rockButton.style.display = "flex"
    scissorButton.style.display = "flex"

    //Reset das cartas escolhidas
    playerChosenCards.style.opacity = 0;
    enemyChosenCards.style.opacity = 0;
    playerChosenCards.className = "playerChosenCards"
    enemyChosenCards.className = "enemyChosenCards"
    playerChosenCards.firstChild.src = "./img/VersoCarta.png"
    enemyChosenCards.firstChild.src = "./img/VersoCarta.png"

    //Reset das palavras
    WordRock.style.opacity = 0;
    WordRock.className = "WordRock"
    WordPaper.style.opacity = 0;
    WordPaper.className = "WordPaper"
    WordsScissor.style.opacity = 0;
    WordsScissor.className = "WordsScissor"
    wordResult.style.opacity = 0;
    wordResult.className = "wordResult"


}



//COMEÇO DO JOGO

let startContainer = document.getElementById('startContainer')
let startVideo = document.getElementById("startVideo")
let loopVideo = document.getElementById("loopVideo")




// musica loop do start 

let startAudioLoop = document.createElement('audio')
startAudioLoop.src = "./music/EntradaeContinuacaoMusic.mp3"
startAudioLoop.loop = true
startAudioLoop.play()
startAudioLoop.autoplay = true

startContainer.appendChild(startAudioLoop)


//VIDEO DE FUNDO - iniciando o game

let startGameVideo = document.createElement('video')
startGameVideo.src = "./video/entradaDoTemplo.mp4"
startGameVideo.autoplay = true
startGameVideo.muted = true
startVideo.appendChild(startGameVideo)



// Video de loop TESTE
let STG2 = document.createElement('video')
STG2.src = "./video/loopdoTemplo.mp4"
STG2.loop = true
STG2.muted = true

setTimeout(() => {
    loopVideo.appendChild(STG2)

    setTimeout(() => {
        loopVideo.style.zIndex = "1"
        STG2.play()
        startVideo.removeChild(startGameVideo)
        setTimeout(() => {
            startGameTitle.style.zIndex = '1'
            loopVideo.style.zIndex = "0"
        }, 500)
    }, 500)

}, 8500)

//BOTÃO DE START - Pos estart game

let startGameTitle = document.getElementById('startGameTitle')


// musica loop do start 

let audioArena = document.createElement('audio')
audioArena.src = "./music/choosingCardMusic.mp3"
audioArena.loop = true

gameDivContainer.appendChild(audioArena)



startGameTitle.addEventListener('click', () => {


    let imgGif = document.createElement("img")
    imgGif.src = "./img/transition.gif"
    gifBox.style.zIndex = "2";
    gifBox.appendChild(imgGif)

    startGameTitle.style.zIndex = '0'

    setTimeout(() => {

        gameDivContainer.style.display = "flex"
    }, 1500)
    setTimeout(() => {
        audioArena.play()
        startAudioLoop.pause()
        gifBox.removeChild(imgGif)
        gifBox.style.zIndex = "0";
    }, 2000)
})







//Animação de personagem

//Animação do mario
let ally = document.getElementById("ally")
ally.removeChild(ally.firstChild)

let imgMario = document.createElement('img')



setInterval(() => {

    imgMario.src = "./img/Mario.png"

    setTimeout(() => {
        imgMario.src = "./img/Mario2.png"
        ally.appendChild(imgMario)
    }, 500)
}, 1000)



//Animação do inimigo

let enemy = document.getElementById("enemy")
enemy.removeChild(enemy.firstChild)

let imgEnemy = document.createElement('img')


setInterval(() => {
    imgEnemy.src = "./img/inimigo2.png"


    setTimeout(() => {
        imgEnemy.src = "./img/inimigo.png"
        enemy.appendChild(imgEnemy)
    }, 2000)
}, 4000)

//Vidas do mario
let marioLifesBox = document.createElement('div')
marioLifesBox.id = "marioLifesBox"
gameDivContainer.appendChild(marioLifesBox)

let marioHead = document.createElement('div')
marioHead.className = "pointDivs"
marioHead.style.backgroundImage = "url('./img/marioHead.png')"
marioLifesBox.appendChild(marioHead)
marioHead.appendChild(document.createElement('h1'))

let coinDiv = document.createElement('div')
coinDiv.className = "pointDivs"
coinDiv.style.backgroundImage = "url('./img/coin.png')"
marioLifesBox.appendChild(coinDiv)
coinDiv.appendChild(document.createElement('h1'))


// Update para ficar resetando valores a cada certo tempo
//E verificar os valores tambem
setInterval(function update() {
    moedas = contador * 10
    marioHead.firstChild.innerHTML = `${vidas}X`
    coinDiv.firstChild.innerHTML = `${moedas}`
    // quando ganha vida, ganha vida a cada 100 moedas
    lifeUp()

    if (vidas == 0) {
        callGameOver()
        vidas += 3
    }
}, 100)


//GAME OVER



const gameOverScreen = document.createElement('div')
gameOverScreen.id = "gameOverScreen"

let marioDerrotado = document.createElement('div')
marioDerrotado.id = "marioDerrotado"

marioDerrotado.style.opacity = 0
marioDerrotado.style.backgroundImage = "url('./img/marioDerrotado.png')"

let wordGameOver = document.createElement('div')
wordGameOver.id = "wordGameOver"

wordGameOver.style.opacity = 0
wordGameOver.style.backgroundImage = "url('./img/gameOver.png')"

let wordRestart = document.createElement('button')
wordRestart.id = "wordRestart"
wordRestart.style.opacity = 0

wordRestart.style.backgroundImage = "url('./img/restart.png')"

let marioDieEffect = document.createElement('audio')
marioDieEffect.src ="./music/marioDie.wav"
gameOverScreen.appendChild(marioDieEffect)

function callGameOver() {
    marioDieEffect.play()
    audioArena.pause() 

    gameDivContainer.appendChild(gameOverScreen)

    gameOverScreen.appendChild(marioDerrotado)

    gameOverScreen.appendChild(wordGameOver)

    gameOverScreen.appendChild(wordRestart)

    setTimeout(() => {
        document.getElementById('gameOverScreen').style.backgroundColor = "black"
    }, 1500)
    setTimeout(() => {
        document.getElementById('gameOverScreen').style.backgroundImage = "url('./img/spotLight.png')"
        gameOverScreen.appendChild(marioDerrotado)

        gameOverScreen.appendChild(wordGameOver)

        gameOverScreen.appendChild(wordRestart)
    }, 2000)
    setTimeout(() => {
        marioDerrotado.className = "Appear"
        marioDerrotado.style.opacity = 1
    }, 2500)
    setTimeout(() => {
        wordGameOver.className = "Appear"
        wordGameOver.style.opacity = 1
    }, 4000)
    setTimeout(() => {
        wordRestart.className = "Appear";
        wordRestart.style.opacity = 1;
    }, 4500)
    setTimeout(() => {
        wordRestart.className = "pisca";
    }, 5000)

}
wordRestart.addEventListener('click', () => {
    document.location.reload(true)
})


